# 
# put your path here
#

train_ds_path = '~/ssd/training.txt'
import os; assert os.path.isfile(train_ds_path) 


#
# main content
#

import numpy as np
import sys
from subprocess import call
from gensim.models.word2vec import Word2Vec
from similarity import LemmaPos2LemmaAdapter
from eval import simlex999, men, wordsim353


class Word2VecSim(object):

    def __init__(self, model_path):
        sys.stderr.write("Loading word2vec model from %s... " %model_path)
        self.w2v = Word2Vec.load_word2vec_format(model_path, binary=True)
        sys.stderr.write("Done.\n")

    def __call__(self, w1, w2):
        try:
            return self.w2v.similarity(w1, w2)
        except KeyError:
            return None

def try_k(k, w2v_version):
    print('\n***\nTrying k=%d, type=%s\n' %(k, w2v_version))
    num_samples = 5
    scores = np.zeros((num_samples, 3))
    for i in range(num_samples):
        vec_path = 'vec.bin'
        call('nice word2vec-%s/word2vec -train %s -output %s -size 200 -window 5 '
             '-sample 1e-4 -negative %d -hs 0 -binary 1 -cbow 0 -iter 3 -threads 16' 
                %(w2v_version, train_ds_path, vec_path, k), shell=True)
        print
        sim = Word2VecSim(vec_path)
        scores[i, 0] = simlex999.evaluate(LemmaPos2LemmaAdapter(sim))[0]
        scores[i, 1] = men.evaluate(LemmaPos2LemmaAdapter(sim))[0]
        scores[i, 2] = wordsim353.evaluate(sim)[0]
        print 'Latest scores: SimLex=%.3f, MEN=%.3f, WordSim=%.3f' %tuple(scores[i])
    print('\nResults for k=%d, type=%s:' %(k, w2v_version))
    print 'SimLex\tMEN\tWordSim'
    print 'Raw scores:' 
    print ''.join('%.3f\t%.3f\t%.3f\n' %tuple(scores[i]) for i in range(num_samples))
    print 'Mean:'
    print '%.3f\t%.3f\t%.3f\n' %tuple(scores.mean(axis=0))
    print 'Standard deviation:'
    print '%.3f\t%.3f\t%.3f\n' %tuple(scores.std(axis=0))


if __name__ == '__main__':
    call('date')
    for k in range(5, 26, 5):
        try_k(k, 'original')
        try_k(k, 'modified')
    call('date')
